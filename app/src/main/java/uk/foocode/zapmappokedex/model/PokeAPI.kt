package uk.foocode.zapmappokedex.model

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import uk.foocode.zapmappokedex.model.data.PokeListResponse
import uk.foocode.zapmappokedex.model.data.PokemonInfo

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */
interface PokeAPI {

    @GET("pokemon")
    suspend fun getPokemonList(@Query("offset") offset:Int, @Query("limit") count:Int) : Response<PokeListResponse>

    @GET("pokemon/{id}/")
    suspend fun getSinglePokemon(@Path("id") id:Int) : Response<PokemonInfo>
}
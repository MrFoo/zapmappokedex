package uk.foocode.zapmappokedex.model

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import uk.foocode.zapmappokedex.BuildConfig
import uk.foocode.zapmappokedex.model.data.PokeListResponse
import uk.foocode.zapmappokedex.model.data.PokemonInfo
import uk.foocode.zapmappokedex.util.apiCall

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */
interface IPokeWebService{
    suspend fun getPokemon(offset:Int,count:Int):Result<PokeListResponse>
    suspend fun getPokemonInfo(id:Int):Result<PokemonInfo>
}

/**
 * Web service to get pokémon lists and information
 */
class PokeWebService(private val api:PokeAPI = Retrofit.Builder().baseUrl(BuildConfig.API_URL).addConverterFactory(
    GsonConverterFactory.create()).build().create(PokeAPI::class.java)) : IPokeWebService {

    override suspend fun getPokemon(offset: Int, count:Int): Result<PokeListResponse> = apiCall { api.getPokemonList(offset,count) }

    override suspend fun getPokemonInfo(id: Int): Result<PokemonInfo> = apiCall { api.getSinglePokemon(id) }
}

package uk.foocode.zapmappokedex.model

import androidx.paging.PagingSource
import androidx.paging.PagingState
import uk.foocode.zapmappokedex.model.data.PokeListItem
import uk.foocode.zapmappokedex.util.ApiException
import java.util.concurrent.TimeoutException

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */
class PokemonSource(private val webService: IPokeWebService = PokeWebService()) : PagingSource<Int, PokeListItem>() {
    override fun getRefreshKey(state: PagingState<Int, PokeListItem>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PokeListItem> {

        val result=webService.getPokemon(params.key?:0,params.loadSize)

        return if(result.isSuccess) {
            result.getOrNull()?.let { list ->
                val next = list.getNextOffset()
                val prev = list.getPrevOffset()
                val retValue=LoadResult.Page(
                    prevKey = prev,
                    nextKey = next,
                    data = list.results
                )
                retValue
            } ?: LoadResult.Error(ApiException("Bad result from API"))
        } else {
            LoadResult.Error(TimeoutException())
        }
    }
}
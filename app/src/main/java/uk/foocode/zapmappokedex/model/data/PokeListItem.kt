package uk.foocode.zapmappokedex.model.data


import com.google.gson.annotations.SerializedName

data class PokeListItem(
    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: String
) {
    fun getId():Int = try { url.split("/").last { it.isNotBlank() }.toInt() } catch(e:Exception) { -1 }
}
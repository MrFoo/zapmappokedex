package uk.foocode.zapmappokedex.model.data


import com.google.gson.annotations.SerializedName

data class PokeListResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("next")
    val next: String?,
    @SerializedName("previous")
    val previous: String?,
    @SerializedName("results")
    val results: List<PokeListItem>
) {
    fun getNextOffset() = next?.substringAfter("offset=","0")?.substringBefore("&","0")?.toInt()
    fun getPrevOffset() = previous?.substringAfter("offset=","0")?.substringBefore("&","0")?.toInt()
}
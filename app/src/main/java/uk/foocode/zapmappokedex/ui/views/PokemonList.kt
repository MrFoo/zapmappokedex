package uk.foocode.zapmappokedex.ui.views

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemsIndexed
import uk.foocode.zapmappokedex.viewmodel.PokeViewModel

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
@Composable
fun PokemonList(navController: NavController, modifier:Modifier=Modifier,pokeViewModel: PokeViewModel = viewModel()){
    val pokeSource = pokeViewModel.pokeSource.collectAsLazyPagingItems()
    LazyColumn(modifier = modifier, contentPadding = PaddingValues(0.dp,16.dp)){
        itemsIndexed(pokeSource){ index,item ->
            item?.let {
                PokemonListItem(name = pokeViewModel.cleanPokemonName(item.name),
                    modifier = Modifier
                        .padding(vertical = 4.dp)
                        .fillMaxWidth()
                        .border(
                        width=5.dp,
                        shape = RoundedCornerShape(5.dp),
                        color = when(index%3){
                            0 -> MaterialTheme.colorScheme.primaryContainer
                            1 -> MaterialTheme.colorScheme.secondaryContainer
                            else -> MaterialTheme.colorScheme.tertiaryContainer
                        })
                        .padding(8.dp)
                        .clickable {
                            navController.navigate("pokeItem/${item.getId()}")
                        })
            }
        }
        pokeSource.apply {
            when{
                loadState.refresh is LoadState.Loading || loadState.append is LoadState.Loading -> {
                    item{
                        Text("LOADING")
                    }
                }
                loadState.refresh is LoadState.Error || loadState.append is LoadState.Error -> {
                    item {
                        Text("Unable to load data. Please check your network connection")
                    }
                }

            }
        }
    }
}

@Composable
fun PokemonListItem(name:String, modifier: Modifier=Modifier){
    Text(text = name,
        style = MaterialTheme.typography.bodyLarge,
        modifier= modifier)
}
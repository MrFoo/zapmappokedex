package uk.foocode.zapmappokedex.ui.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import uk.foocode.zapmappokedex.viewmodel.PokeViewModel

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
@Composable
fun PokeDetails(navController: NavController,pokeID:Int,modifier:Modifier = Modifier,pokeViewModel: PokeViewModel = viewModel()){
    val pokemonDetails by pokeViewModel.getPokemonDetails(pokeID).collectAsState(null)
    val errors by pokeViewModel.errorState.collectAsState()

    Box(modifier = modifier.fillMaxSize()) {
        errors?.let{
            Dialog(onDismissRequest = { navController.popBackStack() }) {
                Text("Error reading from API",
                    modifier = Modifier
                        .background(color = MaterialTheme.colorScheme.background, shape = RoundedCornerShape(12.dp))
                        .padding(32.dp),
                    style = MaterialTheme.typography.titleLarge)
            }
        }
        pokemonDetails?.let{ details ->
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 16.dp)) {
                Text(text = pokeViewModel.cleanPokemonName(details.name), style = MaterialTheme.typography.titleLarge)
                Row {
                    AsyncImage(
                        model = details.sprites.frontDefault,
                        contentDescription = "Picture of ${details.name}",
                        modifier = Modifier
                            .size(128.dp)
                            .clip(RoundedCornerShape(4.dp))
                            .border(
                                width = 2.dp,
                                color = MaterialTheme.colorScheme.primaryContainer,
                                shape = RoundedCornerShape(4.dp)
                            ),
                        contentScale = ContentScale.FillBounds
                    )
                    Column(modifier = Modifier.padding(start=16.dp)) {
                        Text("Types:", style = MaterialTheme.typography.titleSmall)
                        details.types.forEach { type ->
                            Text(type.type.name, style = MaterialTheme.typography.labelMedium)
                        }
                    }

                }
                Text("Weight: ${pokeViewModel.formatPokemonWeight(details.weight)}kg")
                Text("Height: ${pokeViewModel.formatPokemonHeight(details.height)}cm")
            }
        }
        Image(painter = painterResource(id = android.R.drawable.ic_menu_close_clear_cancel),
            contentDescription = "back",
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(end = 12.dp, top = 12.dp)
                .background(
                    color = MaterialTheme.colorScheme.primary,
                    shape = RoundedCornerShape(2.dp)
                )
                .clickable {
                    navController.popBackStack()
                }
                .padding(12.dp))
    }
}
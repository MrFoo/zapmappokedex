package uk.foocode.zapmappokedex.util

import retrofit2.Response
import java.lang.Exception

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */

suspend fun <T:Any> apiCall(method:suspend ()-> Response<T>) : Result<T> {
    return try {
        val result = method()
        if (result.isSuccessful && result.body() != null) {
            Result.success(result.body()!!)
        } else {
            Result.failure(ApiException(result.errorBody()?.string() ?: "Unknown API Exception"))
        }
    }catch(e: Exception){
        Result.failure(e)
    }
}

class ApiException(message:String) : Exception(message)
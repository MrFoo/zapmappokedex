package uk.foocode.zapmappokedex.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import uk.foocode.zapmappokedex.model.IPokeWebService
import uk.foocode.zapmappokedex.model.PokeWebService
import uk.foocode.zapmappokedex.model.PokemonSource

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */
class PokeViewModel(private val webService: IPokeWebService = PokeWebService()) : ViewModel() {

    // No simple way to test this so not given as argument.
    val pokeSource= Pager(PagingConfig(pageSize = 20)) { PokemonSource() }.flow.cachedIn(viewModelScope)

    val errorState = MutableStateFlow<String?>(null)

    fun getPokemonDetails(id:Int) = flow {
        val apiResult = webService.getPokemonInfo(id)
        if (apiResult.isSuccess) {
            emit(apiResult.getOrNull())
        } else {
            errorState.emit(apiResult.exceptionOrNull()?.toString())
        }
    }

    // API returns data in form text-text, convert to Text-Text
    fun cleanPokemonName(name:String) = name
        .split("-")
        .joinToString("-") {
            it.replaceFirstChar { it.uppercase() }
        }

    // API returns the weight in 10ths of a Kg
    fun formatPokemonWeight(weight:Int) = String.format("%.1f",weight/10f)

    // API returns height in 10ths of a meter
    fun formatPokemonHeight(height:Int) = String.format("%d",height*10)
}
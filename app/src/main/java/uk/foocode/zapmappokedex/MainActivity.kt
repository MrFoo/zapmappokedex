package uk.foocode.zapmappokedex

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import uk.foocode.zapmappokedex.ui.theme.ZapMapPokedexTheme
import uk.foocode.zapmappokedex.ui.views.PokeDetails
import uk.foocode.zapmappokedex.ui.views.PokemonList

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ZapMapPokedexTheme {
                val navController = rememberNavController()
                NavHost(navController = navController, startDestination = "pokeList"){
                    composable("pokeList"){
                        PokemonList(navController = navController, modifier = Modifier.fillMaxSize().padding(horizontal = 16.dp))
                    }
                    composable(
                        route = "pokeItem/{pokeId}",
                        arguments = listOf(navArgument("pokeId"){ type = NavType.IntType})){ backStackEntry ->
                        PokeDetails(navController = navController, pokeID = backStackEntry.arguments?.getInt("pokeId") ?: 1)
                    }
                }
            }
        }
    }
}
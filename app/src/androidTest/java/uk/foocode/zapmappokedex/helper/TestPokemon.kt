package uk.foocode.zapmappokedex.helper

import uk.foocode.zapmappokedex.model.data.PokemonInfo
import uk.foocode.zapmappokedex.model.data.Sprites
import uk.foocode.zapmappokedex.model.data.Type
import uk.foocode.zapmappokedex.model.data.TypeX

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
val testPokemon= PokemonInfo(
    listOf(),
    0,
    listOf(),
    listOf(),
    12,
    listOf(),
    1,
    false,
    "",
    listOf(),
    "test-pokemon-name",
    1,
    listOf(),
    "",
    Sprites("","","","","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/10228.png","","","","",""),
    listOf(),
    listOf(Type(1, TypeX("type1","")), Type(2, TypeX("type2",""))),
    1234
)

val goodTestPokemonName="Test-Pokemon-Name"
val goodTestPokemonWeight="123.4"
val goodTestPokemonHeight="120"
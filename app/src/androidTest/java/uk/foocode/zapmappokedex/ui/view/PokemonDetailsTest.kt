package uk.foocode.zapmappokedex.ui.view

import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.compose.rememberNavController
import androidx.test.espresso.matcher.ViewMatchers.withText
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.foocode.zapmappokedex.helper.goodTestPokemonHeight
import uk.foocode.zapmappokedex.helper.goodTestPokemonName
import uk.foocode.zapmappokedex.helper.goodTestPokemonWeight
import uk.foocode.zapmappokedex.helper.testPokemon
import uk.foocode.zapmappokedex.model.IPokeWebService
import uk.foocode.zapmappokedex.model.data.*
import uk.foocode.zapmappokedex.ui.views.PokeDetails
import uk.foocode.zapmappokedex.viewmodel.PokeViewModel

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
class PokemonDetailsTest {

    private lateinit var mockWebService: IPokeWebService


    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUp(){
        mockWebService = object :IPokeWebService {
            override suspend fun getPokemon(offset: Int, count: Int): Result<PokeListResponse> {
                // not used
                return Result.success(PokeListResponse(1,"","", listOf()))
            }

            override suspend fun getPokemonInfo(id: Int): Result<PokemonInfo> {
                return Result.success(testPokemon)
            }
        }
    }


    @Test
    fun showPokemonDetailsCorrectly():Unit = runBlocking {
        composeTestRule.setContent {
            PokeDetails(navController = rememberNavController(), pokeID = 1, pokeViewModel = PokeViewModel(mockWebService))
        }
        delay(1000)
        composeTestRule.onNode(hasText(goodTestPokemonName)).assertExists("Pokemon name incorrect")
        composeTestRule.onNode(hasText(goodTestPokemonHeight, substring = true)).assertExists("Pokemon Height incorrect")
        composeTestRule.onNode(hasText(goodTestPokemonWeight,substring = true)).assertExists("Pokemon wieght incorrect")
    }
}
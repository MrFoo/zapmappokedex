package uk.foocode.zapmappokedex.ui.view

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import uk.foocode.zapmappokedex.ui.views.PokemonList
import uk.foocode.zapmappokedex.ui.views.PokemonListItem

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
class PokemonListTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun showsCorrectPokemonName():Unit = runBlocking {
        composeTestRule.setContent {
            PokemonListItem(name = "My-Name-Here")
        }
        composeTestRule.onNode(hasText("My-Name-Here")).assertExists("No view has given text")
    }

    @Test
    fun showsListOfPokemon():Unit = runBlocking {
        composeTestRule.setContent {
            PokemonList(navController = rememberNavController())
        }
        delay(1000)
        composeTestRule.onNode(hasScrollAction()).onChildAt(0).assertExists("Should show at least 1 item")
        composeTestRule.onNode(hasScrollAction()).onChildAt(3).assertExists("Should show at least a few pokemon")
    }
}
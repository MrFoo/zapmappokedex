package uk.foocode.zapmappokedex.util

import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Test
import retrofit2.Response
import java.util.concurrent.TimeoutException
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
class ApiCallTest {

    @Test
    fun `Return success on good response`() = runBlocking {
        val result = apiCall { Response.success("abc".toResponseBody("text/html".toMediaType())) }
        assertTrue(result.isSuccess,"Should respond with success")
    }

    @Test
    fun `Blank response body returns failure`() = runBlocking {
        val result = apiCall { Response.success(null) }
        assertTrue(result.isFailure,"Should respond with failure")
    }

    @Test
    fun `Returns failure on bad response`() = runBlocking {
        val result = apiCall { Response.error(404,"Not Found".toResponseBody("text/html".toMediaType())) }
        assertTrue(result.isFailure,"Should respond with failure")
        val response =result.exceptionOrNull()
        assertNotNull(response)
        assertTrue(response is ApiException)
    }

    @Test
    fun `Returns failure on exception`() = runBlocking {
        val result = apiCall<Response<Any>> { throw TimeoutException() }
        assertTrue(result.isFailure,"Should respond with failure")
        val response = result.exceptionOrNull()
        assertNotNull(response)
        assertTrue(response is TimeoutException)
    }

}
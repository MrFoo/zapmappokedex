package uk.foocode.zapmappokedex.model.data

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */
class PokeListResponseTest {

    val goodResponse=PokeListResponse(1126,"https://pokeapi.co/api/v2/pokemon?offset=60&limit=20","https://pokeapi.co/api/v2/pokemon?offset=20&limit=20",
        listOf())

    @Test
    fun `Can get previous offset`(){
        assertEquals(20,goodResponse.getPrevOffset())
    }

    @Test
    fun `Can get next offset`(){
        assertEquals(60,goodResponse.getNextOffset())
    }
}
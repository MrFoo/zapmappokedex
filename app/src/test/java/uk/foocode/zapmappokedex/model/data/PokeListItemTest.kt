package uk.foocode.zapmappokedex.model.data

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */
class PokeListItemTest {

    private val goodName="charmander"

    private val goodPokemon = PokeListItem(goodName,"https://pokeapi.co/api/v2/pokemon/4/")
    private val badPokemon = PokeListItem("","")


    @Test
    fun getId() {
        assertEquals(4,goodPokemon.getId())
        assertEquals(-1,badPokemon.getId())
    }

    @Test
    fun getName() {
        assertEquals(goodName,goodPokemon.name)
        assertTrue(badPokemon.name.isEmpty())
    }
}
package uk.foocode.zapmappokedex.model

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uk.foocode.zapmappokedex.util.ApiException
import kotlin.jvm.Throws
import kotlin.test.*

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */

class PokeAPITest {

    private lateinit var client:PokeAPI

    private val GOOD_POKEMON_ID = 1
    private val GOOD_POKEMON_NAME = "bulbasaur"

    private val BAD_POKEMON_ID = -1

    @Before
    fun setUp(){
        client=Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/").addConverterFactory(GsonConverterFactory.create()).build().create(PokeAPI::class.java)
    }

    @Test
    fun `Can get list of Pokemon`() = runBlocking {
        val result=client.getPokemonList(0,10)
        assertTrue(result.isSuccessful,"Request failed")
        assertNotNull(result.body(),"Result body null")
        assertTrue(result.body()!!.results.isNotEmpty(),"Result count is 0")
    }

    @Test
    fun `Can get specific number of Pokemon`() = runBlocking {
        val result=client.getPokemonList(0,10)
        assertTrue(result.isSuccessful,"Request failed")
        assertNotNull(result.body(),"Result body null")
        assertEquals(10,result.body()!!.results.size ,"Does not contain 10 entries.")
    }

    @Test
    fun `Can get further list of Pokemon`() = runBlocking {
        val result=client.getPokemonList(20,20)
        assertTrue(result.isSuccessful,"Request failed")
        assertNotNull(result.body(),"Result body null")
        assertTrue(result.body()!!.count > 0,"Result count is 0")
        assertTrue(result.body()!!.results.first().getId()>15)
    }

    @Test
    fun `Has error on bad Id`() = runBlocking {
        val result = client.getSinglePokemon(BAD_POKEMON_ID)
        assertFalse(result.isSuccessful,"Should give error for bad ID")
    }

    @Test
    fun `Can get single pokemon`() = runBlocking {
        val result=client.getSinglePokemon(GOOD_POKEMON_ID)
        assertTrue(result.isSuccessful,"Request failed")
        assertNotNull(result.body(),"Result body null")
        assert(result.body()!!.name==GOOD_POKEMON_NAME)
    }

    @Test
    fun `Can get single later pokemon`() = runBlocking {
        val result=client.getSinglePokemon(10228)
        assertTrue(result.isSuccessful,"Request failed")
        assertNotNull(result.body(),"Result body null")
        assert(result.body()!!.name=="toxtricity-low-key-gmax")
    }

}
package uk.foocode.zapmappokedex.model

import com.google.gson.Gson
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockkClass
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.create
import uk.foocode.zapmappokedex.model.data.*
import uk.foocode.zapmappokedex.util.ApiException
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 13/06/2022.
 */

class PokeWebServiceIntegrationTest {

    private lateinit var webService: IPokeWebService

    private val GOOD_POKEMON_ID = 1
    private val BAD_POKEMON_ID = -1

    private val GOOD_OFFSET = 0
    private val BAD_OFFSET = -1

    @Before
    fun setUp(){
        webService=PokeWebService()
    }

    @Test
    fun `Can get list of Pokemon`() = runBlocking {
        val result = webService.getPokemon(0,20)
        assertTrue(result.isSuccess)
        val pokemon=result.getOrNull()
        assertNotNull(pokemon,"Result should not be null")
        assertFalse(pokemon.results.isEmpty(),"Result should have child pokemon")
    }

    @Test
    fun `Can get specific number of Pokemon`() = runBlocking {
        val result = webService.getPokemon(0,20)
        assertTrue(result.isSuccess)
        val pokemon=result.getOrNull()
        assertNotNull(pokemon,"Result should not be null")
        assertFalse(pokemon.results.isEmpty(),"Result should have child pokemon")
        assertEquals(20,pokemon.results.size,"Does not have 20 pokemon")
    }

    @Test
    fun `Can get single Pokemon`() = runBlocking {
        val result = webService.getPokemonInfo(GOOD_POKEMON_ID)
        assertTrue(result.isSuccess)
        val pokemon=result.getOrNull()
        assertNotNull(pokemon)
        assertEquals("bulbasaur",pokemon.name)
    }

    @Test
    fun `Returns error for bad Id`()= runBlocking {
        val result = webService.getPokemonInfo(BAD_POKEMON_ID)
        assertTrue(result.isFailure)
        assertTrue(result.exceptionOrNull() is ApiException)
    }
}
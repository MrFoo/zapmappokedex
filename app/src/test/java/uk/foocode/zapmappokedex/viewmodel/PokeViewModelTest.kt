package uk.foocode.zapmappokedex.viewmodel

import com.google.gson.Gson
import io.mockk.coEvery
import io.mockk.mockkClass
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import uk.foocode.zapmappokedex.viewmodel.PokeViewModel
import uk.foocode.zapmappokedex.model.IPokeWebService
import uk.foocode.zapmappokedex.model.data.PokeListResponse
import uk.foocode.zapmappokedex.model.data.PokemonInfo
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * Created by John Gilbertson (jgilbert@liverpool.ac.uk) on 14/06/2022.
 */
class PokeViewModelTest {

    private val mockWebService = mockkClass(IPokeWebService::class)

    private lateinit var viewModel: PokeViewModel

    @Before
    fun setUp(){
        coEvery { mockWebService.getPokemon(any(),any()) } returns Result.success(Gson().fromJson("""{"count":1126,"next":"https://pokeapi.co/api/v2/pokemon?offset=20&limit=20","previous":null,"results":[{"name":"bulbasaur","url":"https://pokeapi.co/api/v2/pokemon/1/"},{"name":"ivysaur","url":"https://pokeapi.co/api/v2/pokemon/2/"},{"name":"venusaur","url":"https://pokeapi.co/api/v2/pokemon/3/"},{"name":"charmander","url":"https://pokeapi.co/api/v2/pokemon/4/"},{"name":"charmeleon","url":"https://pokeapi.co/api/v2/pokemon/5/"},{"name":"charizard","url":"https://pokeapi.co/api/v2/pokemon/6/"},{"name":"squirtle","url":"https://pokeapi.co/api/v2/pokemon/7/"},{"name":"wartortle","url":"https://pokeapi.co/api/v2/pokemon/8/"},{"name":"blastoise","url":"https://pokeapi.co/api/v2/pokemon/9/"},{"name":"caterpie","url":"https://pokeapi.co/api/v2/pokemon/10/"},{"name":"metapod","url":"https://pokeapi.co/api/v2/pokemon/11/"},{"name":"butterfree","url":"https://pokeapi.co/api/v2/pokemon/12/"},{"name":"weedle","url":"https://pokeapi.co/api/v2/pokemon/13/"},{"name":"kakuna","url":"https://pokeapi.co/api/v2/pokemon/14/"},{"name":"beedrill","url":"https://pokeapi.co/api/v2/pokemon/15/"},{"name":"pidgey","url":"https://pokeapi.co/api/v2/pokemon/16/"},{"name":"pidgeotto","url":"https://pokeapi.co/api/v2/pokemon/17/"},{"name":"pidgeot","url":"https://pokeapi.co/api/v2/pokemon/18/"},{"name":"rattata","url":"https://pokeapi.co/api/v2/pokemon/19/"},{"name":"raticate","url":"https://pokeapi.co/api/v2/pokemon/20/"}]}""", PokeListResponse::class.java))
        coEvery { mockWebService.getPokemonInfo(any()) } returns Result.success(Gson().fromJson("""{"abilities":[{"ability":{"name":"punk-rock","url":"https://pokeapi.co/api/v2/ability/244/"},"is_hidden":false,"slot":1},{"ability":{"name":"minus","url":"https://pokeapi.co/api/v2/ability/58/"},"is_hidden":false,"slot":2},{"ability":{"name":"technician","url":"https://pokeapi.co/api/v2/ability/101/"},"is_hidden":true,"slot":3}],"base_experience":176,"forms":[{"name":"toxtricity-low-key-gmax","url":"https://pokeapi.co/api/v2/pokemon-form/10397/"}],"game_indices":[],"height":240,"held_items":[],"id":10228,"is_default":false,"location_area_encounters":"https://pokeapi.co/api/v2/pokemon/10228/encounters","moves":[],"name":"toxtricity-low-key-gmax","order":-1,"past_types":[],"species":{"name":"toxtricity","url":"https://pokeapi.co/api/v2/pokemon-species/849/"},"sprites":{"back_default":null,"back_female":null,"back_shiny":null,"back_shiny_female":null,"front_default":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/10228.png","front_female":null,"front_shiny":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/10228.png","front_shiny_female":null,"other":{"dream_world":{"front_default":null,"front_female":null},"home":{"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null},"official-artwork":{"front_default":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/10228.png"}},"versions":{"generation-i":{"red-blue":{"back_default":null,"back_gray":null,"back_transparent":null,"front_default":null,"front_gray":null,"front_transparent":null},"yellow":{"back_default":null,"back_gray":null,"back_transparent":null,"front_default":null,"front_gray":null,"front_transparent":null}},"generation-ii":{"crystal":{"back_default":null,"back_shiny":null,"back_shiny_transparent":null,"back_transparent":null,"front_default":null,"front_shiny":null,"front_shiny_transparent":null,"front_transparent":null},"gold":{"back_default":null,"back_shiny":null,"front_default":null,"front_shiny":null,"front_transparent":null},"silver":{"back_default":null,"back_shiny":null,"front_default":null,"front_shiny":null,"front_transparent":null}},"generation-iii":{"emerald":{"front_default":null,"front_shiny":null},"firered-leafgreen":{"back_default":null,"back_shiny":null,"front_default":null,"front_shiny":null},"ruby-sapphire":{"back_default":null,"back_shiny":null,"front_default":null,"front_shiny":null}},"generation-iv":{"diamond-pearl":{"back_default":null,"back_female":null,"back_shiny":null,"back_shiny_female":null,"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null},"heartgold-soulsilver":{"back_default":null,"back_female":null,"back_shiny":null,"back_shiny_female":null,"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null},"platinum":{"back_default":null,"back_female":null,"back_shiny":null,"back_shiny_female":null,"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null}},"generation-v":{"black-white":{"animated":{"back_default":null,"back_female":null,"back_shiny":null,"back_shiny_female":null,"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null},"back_default":null,"back_female":null,"back_shiny":null,"back_shiny_female":null,"front_default":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/10228.png","front_female":null,"front_shiny":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/shiny/10228.png","front_shiny_female":null}},"generation-vi":{"omegaruby-alphasapphire":{"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null},"x-y":{"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null}},"generation-vii":{"icons":{"front_default":null,"front_female":null},"ultra-sun-ultra-moon":{"front_default":null,"front_female":null,"front_shiny":null,"front_shiny_female":null}},"generation-viii":{"icons":{"front_default":null,"front_female":null}}}},"stats":[{"base_stat":75,"effort":0,"stat":{"name":"hp","url":"https://pokeapi.co/api/v2/stat/1/"}},{"base_stat":98,"effort":0,"stat":{"name":"attack","url":"https://pokeapi.co/api/v2/stat/2/"}},{"base_stat":70,"effort":0,"stat":{"name":"defense","url":"https://pokeapi.co/api/v2/stat/3/"}},{"base_stat":114,"effort":2,"stat":{"name":"special-attack","url":"https://pokeapi.co/api/v2/stat/4/"}},{"base_stat":70,"effort":0,"stat":{"name":"special-defense","url":"https://pokeapi.co/api/v2/stat/5/"}},{"base_stat":75,"effort":0,"stat":{"name":"speed","url":"https://pokeapi.co/api/v2/stat/6/"}}],"types":[{"slot":1,"type":{"name":"electric","url":"https://pokeapi.co/api/v2/type/13/"}},{"slot":2,"type":{"name":"poison","url":"https://pokeapi.co/api/v2/type/4/"}}],"weight":10000}""", PokemonInfo::class.java))
        viewModel = PokeViewModel(mockWebService)
    }


    @Test
    fun `Can get single Pokemon`() = runBlocking {
        val info = viewModel.getPokemonDetails(1)

        val results= mutableListOf<PokemonInfo>()

        val job = launch {
            info.collectLatest { result ->
                result?.let { results.add(it) }
            }
        }
        delay(100)
        assertTrue(results.isNotEmpty())
        assertEquals("toxtricity-low-key-gmax",results.first().name)
        job.cancel()
    }

    @Test
    fun `Converts Pokemon names correctly`(){
        val goodName="Pokemon-Name-Capitalised"
        val result = viewModel.cleanPokemonName(goodName.lowercase())
        assertEquals(goodName,result,"Names do not match")
    }

    @Test
    fun `Converts Pokemon height correctly`(){
        val goodHeight="100"
        val apiHeight=10
        assertEquals(goodHeight,viewModel.formatPokemonHeight(apiHeight),"Height does not match")
    }

    @Test
    fun `Converts Pokemon weight correctly`(){
        val goodWeight="102.3"
        val apiWeight=1023
        assertEquals(goodWeight,viewModel.formatPokemonWeight(apiWeight),"Weight does not match")
    }
}